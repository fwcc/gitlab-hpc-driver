set(DOXYGEN_QUIET                  NO)
set(DOXYGEN_GENERATE_XML           YES)
set(DOXYGEN_GENERATE_HTML          NO)
set(DOXYGEN_GENERATE_LATEX         NO)
set(DOXYGEN_GENERATE_MAN           NO)
set(DOXYGEN_EXTRACT_PRIVATE        YES)
set(DOXYGEN_EXTRACT_PACKAGE        YES)
set(DOXYGEN_EXTRACT_STATIC         YES)
set(DOXYGEN_EXTRACT_LOCAL_METHODS  YES)
set(DOXYGEN_WARN_IF_UNDOCUMENTED   YES)
set(DOXYGEN_WARN_NO_PARAMDOC       YES)

doxygen_add_docs(xml
    ../src
    COMMENT "Generating Doxygen XML"
)

set(SPHINX_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
set(SPHINX_BUILD_DIR  "${CMAKE_CURRENT_BINARY_DIR}")

# TODO: Build out of source when Exhale allows this
set(EXHALE_CONTAINMENT_FOLDER source_code_doc)

add_custom_target(docs
    COMMAND "${SPHINX_EXECUTABLE}"
        -b html

        # sphinx parameters
        -Drelease="${PROJECT_VERSION}"

        # breathe parameters
        -Dbreathe_projects.driver="${SPHINX_BUILD_DIR}/xml"

        # exhale parameters
        -Dexhale_args.containmentFolder="${EXHALE_CONTAINMENT_FOLDER}" # must be in source dir :-/

        "${SPHINX_SOURCE_DIR}" "${SPHINX_BUILD_DIR}"
    DEPENDS xml
    COMMENT "Generating documentation"
)
