from exhale import utils

# show private members as well; see exhale_args
def specificationsForKind(kind):
    '''
    For a given input ``kind``, return the list of reStructuredText specifications
    for the associated Breathe directive.
    '''
    # Change the defaults for .. doxygenclass:: and .. doxygenstruct::
    if kind == "class" or kind == "struct":
        return [
          ":members:",
          ":protected-members:",
          ":private-members:",
          ":undoc-members:"
        ]
    # Change the defaults for .. doxygenenum::
    elif kind == "enum":
        return [":no-link:"]
    # An empty list signals to Exhale to use the defaults
    else:
        return []

# -- Project information -----------------------------------------------------

# will be set by CMake
project = 'GitLab HPC Runner'
copyright = ''
author = ''

release = ''

# -- General configuration ---------------------------------------------------

extensions = ['recommonmark', 'breathe', 'exhale']

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
    '.markdown': 'markdown'
}

# will be set by CMake
breathe_projects = { 'driver': '' }
breathe_default_project = 'driver'

exhale_args = {
    "containmentFolder":     "",
    "rootFileName":          "source_code_doc.rst",
    "rootFileTitle":         "Source Code Documentation",
    "doxygenStripFromPath":  "..",
    "createTreeView":        True,
    "exhaleExecutesDoxygen": False,
    "customSpecificationsMapping": utils.makeCustomSpecificationsMapping(specificationsForKind)
}

primary_domain = 'cpp'
highlight_language = 'cpp'
templates_path = [ '_templates' ]
exclude_patterns = []
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
