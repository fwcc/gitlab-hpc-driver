# Installation

The program can be installed using CMake.

```bash
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=<install-prefix> ..
make
make install
```

# Configuration

The program will read the configuration file located at `~/.config/gitlab-hpc-driver.cfg`. You can
configure the following variables:

- `unified_output` (boolean)
  - unify stdout and stderr into a single stream → chronologically correct output but everything
    over stdout
  - possible values: **true**, false
- `logging_directory` (string)
  - specifies where log files will be written
  - example: `logging_directory = "/var/logs/hpc-driver/"`
- `log_level` (string)
  - specifies the level of detail of the logs
  - possible values: trace, debug, **info**, warning, error, critical, off, null (= off)
  - example: `log_level = "warning"`

