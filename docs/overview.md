This is a driver for the GitLab runner custom executor that allows users to run their CI jobs on HPC
clusters. Currently, the implementation utilizes the batch system [Slurm](https://slurm.schedmd.com/)
but support for other systems may be implemented later.

# Architecture

The architecture of this project is predetermined by GitLab. A basic setup needs a GitLab instance
and a machine (login/head node of the cluster) running the
[GitLab runner](https://docs.gitlab.com/runner/) service.

<!-- TODO: Diagram of components: GitLab, Runner service, Runners -->

The runner service polls GitLab for new CI jobs. GitLab manages and prepares these jobs for all its
users. The runner service has a `config.toml` file that describes its runners. Each runner has tags
associated with it that can be specified when registering the runner with GitLab. Based on these
tags, GitLab can choose a runner to pass a CI job to -- depending on the tags a user specified in
their project's `.gitlab-ci.yml`. The runner passes the job to its driver which then executes the
job.

Runners in general can utilize various executors. This project was implemented using the
[custom executor](https://docs.gitlab.com/runner/executors/custom.html) which allows for a custom
driver to be specified to deal with the CI jobs. The runner service generates multiple bash scripts
for each job that are then passed to the custom program when calling it.

A CI job is split into four stages:

- config stage -- configures the job based on runtime information
- prepare stage -- prepares the environment for job execution (not used in this implementation)
- run stage -- executes the job (obligatory!)
- cleanup stage -- clean up the environment.

The prepare stage is not used because a meaningful seperation between it and the run stage is not
possible without losing your Slurm resource allocation.

The run stage is executed multiple times with the following sub-stages:

1. `prepare_script`
2. `get_sources`
3. `restore_cache`
4. `download_artifacts`
5. `build_script` -- Combination of `before_script` and `script` from `.gitlab-ci.yml` → The only
   sub-stage to be run as a Slurm job!
6. `after_script`
7. `archive_cache` / `archive_cache_on_failure`
8. `upload_artifacts_on_success` / `upload_artifacts_on_failure`
9. `cleanup_file_variables`

Before and after the `build_script` there may be multiple sub-stages `step_*`.

In the config and cleanup stages the runner service calls the custom driver directly. In the run
stage it additionally passes the path to a bash script and the name of the current sub-stage to the
executable. For this project the three job stages that are implemented use the same program for
their execution. To allow it to distinguish between them, it is meant to be passed the name of the
stage as first argument. This can be achieved by using the `<stage>_args` variable in the
`config.toml`. More information on configuring runners can be found in the
[GitLab Docs](https://docs.gitlab.com/runner/executors/custom.html#configuration).

In summary, a suitable configuration for an HPC runner using this driver may look something like
this:

```toml
[[runners]]
  name = "My HPC Runner"
  url = "https://gitlab.hzdr.de/"
  token = "xxxxxxxxxxxxxxxxxxxx"
  executor = "custom"
  builds_dir = "/home/gitlab-runner/builds"
  cache_dir = "/home/gitlab-runner/cache"
  [runners.custom]
    config_exec = "gitlab-hpc-runner"
    config_args = ["config"]
    run_exec = "gitlab-hpc-runner"
    run_args = ["run"]
    cleanup_exec = "gitlab-hpc-runner"
    cleanup_args = ["cleanup"]
```

<!-- # Design -->

<!-- TODO: -->
<!-- Explain classes, hierarchy, factories etc. -->
<!-- Add UML diagrams -->
