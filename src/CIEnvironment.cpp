#include "CIEnvironment.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "spdlog/spdlog.h"

CIEnvironment::CIEnvironment()
{
    const char* esf = std::getenv("SYSTEM_FAILURE_EXIT_CODE");
    const char* ebf = std::getenv("BUILD_FAILURE_EXIT_CODE");

    if (esf && ebf) {
        std::sscanf(esf, "%d", &ExitSystemFailure);
        std::sscanf(ebf, "%d", &ExitBuildFailure);
        SPDLOG_DEBUG("SYSTEM_FAILURE_EXIT_CODE={}, BUILD_FAILURE_EXIT_CODE={}",
            ExitSystemFailure,
            ExitBuildFailure);
    } else {
        ExitSystemFailure = 2;
        ExitBuildFailure = 1;
        SPDLOG_WARN("gitlab-runner did not provide SYSTEM_FAILURE_EXIT_CODE and/or "
                    "BUILD_FAILURE_EXIT_CODE environment variables. Using return "
                    "values {} and {} "
                    "instead.",
            ExitSystemFailure,
            ExitBuildFailure);
    }

    const char* home = std::getenv("HOME");
    if (home)
        Home = std::filesystem::path(home);
    else {
        SPDLOG_ERROR("Could not get home directory");
        std::exit(ExitSystemFailure);
    }

    std::vector<std::string> keys(
        { "CUSTOM_ENV_ARTIFACT_DOWNLOAD_ATTEMPTS",
            "CUSTOM_ENV_CHAT_INPUT",
            "CUSTOM_ENV_CHAT_CHANNEL",
            "CUSTOM_ENV_CI",
            "CUSTOM_ENV_CI_BUILDS_DIR",
            "CUSTOM_ENV_CI_CONCURRENT_ID",
            "CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID",
            "CUSTOM_ENV_CI_COMMIT_BEFORE_SHA",
            "CUSTOM_ENV_CI_COMMIT_DESCRIPTION",
            "CUSTOM_ENV_CI_COMMIT_MESSAGE",
            "CUSTOM_ENV_CI_COMMIT_REF_NAME",
            "CUSTOM_ENV_CI_COMMIT_REF_SLUG",
            "CUSTOM_ENV_CI_COMMIT_SHA",
            "CUSTOM_ENV_CI_COMMIT_SHORT_SHA",
            "CUSTOM_ENV_CI_COMMIT_TAG",
            "CUSTOM_ENV_CI_COMMIT_TITLE",
            "CUSTOM_ENV_CI_CONFIG_PATH",
            "CUSTOM_ENV_CI_DEBUG_TRACE",
            "CUSTOM_ENV_CI_DEPLOY_PASSWORD",
            "CUSTOM_ENV_CI_DEPLOY_USER",
            "CUSTOM_ENV_CI_DISPOSABLE_ENVIRONMENT",
            "CUSTOM_ENV_CI_ENVIRONMENT_NAME",
            "CUSTOM_ENV_CI_ENVIRONMENT_SLUG",
            "CUSTOM_ENV_CI_ENVIRONMENT_URL",
            "CUSTOM_ENV_CI_DEFAULT_BRANCH",
            "CUSTOM_ENV_CI_JOB_ID",
            "CUSTOM_ENV_CI_JOB_MANUAL",
            "CUSTOM_ENV_CI_JOB_NAME",
            "CUSTOM_ENV_CI_JOB_STAGE",
            "CUSTOM_ENV_CI_JOB_TOKEN",
            "CUSTOM_ENV_CI_JOB_URL",
            "CUSTOM_ENV_CI_MERGE_REQUEST_ID",
            "CUSTOM_ENV_CI_MERGE_REQUEST_IID",
            "CUSTOM_ENV_CI_MERGE_REQUEST_PROJECT_ID",
            "CUSTOM_ENV_CI_MERGE_REQUEST_PROJECT_PATH",
            "CUSTOM_ENV_CI_MERGE_REQUEST_PROJECT_URL",
            "CUSTOM_ENV_CI_MERGE_REQUEST_REF_PATH",
            "CUSTOM_ENV_CI_MERGE_REQUEST_SOURCE_BRANCH_NAME",
            "CUSTOM_ENV_CI_MERGE_REQUEST_SOURCE_BRANCH_SHA",
            "CUSTOM_ENV_CI_MERGE_REQUEST_SOURCE_PROJECT_ID",
            "CUSTOM_ENV_CI_MERGE_REQUEST_SOURCE_PROJECT_PATH",
            "CUSTOM_ENV_CI_MERGE_REQUEST_SOURCE_PROJECT_URL",
            "CUSTOM_ENV_CI_MERGE_REQUEST_TARGET_BRANCH_NAME",
            "CUSTOM_ENV_CI_MERGE_REQUEST_TARGET_BRANCH_SHA",
            "CUSTOM_ENV_CI_MERGE_REQUEST_TITLE",
            "CUSTOM_ENV_CI_MERGE_REQUEST_ASSIGNEES",
            "CUSTOM_ENV_CI_MERGE_REQUEST_MILESTONE",
            "CUSTOM_ENV_CI_MERGE_REQUEST_LABELS",
            "CUSTOM_ENV_CI_EXTERNAL_PULL_REQUEST_IID",
            "CUSTOM_ENV_CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA",
            "CUSTOM_ENV_CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA",
            "CUSTOM_ENV_CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME",
            "CUSTOM_ENV_CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME",
            "CUSTOM_ENV_CI_NODE_INDEX",
            "CUSTOM_ENV_CI_NODE_TOTAL",
            "CUSTOM_ENV_CI_API_V4_URL",
            "CUSTOM_ENV_CI_PAGES_DOMAIN",
            "CUSTOM_ENV_CI_PAGES_URL",
            "CUSTOM_ENV_CI_PIPELINE_ID",
            "CUSTOM_ENV_CI_PIPELINE_IID",
            "CUSTOM_ENV_CI_PIPELINE_SOURCE",
            "CUSTOM_ENV_CI_PIPELINE_TRIGGERED",
            "CUSTOM_ENV_CI_PIPELINE_URL",
            "CUSTOM_ENV_CI_PROJECT_DIR",
            "CUSTOM_ENV_CI_PROJECT_ID",
            "CUSTOM_ENV_CI_PROJECT_NAME",
            "CUSTOM_ENV_CI_PROJECT_TITLE",
            "CUSTOM_ENV_CI_PROJECT_NAMESPACE",
            "CUSTOM_ENV_CI_PROJECT_PATH",
            "CUSTOM_ENV_CI_PROJECT_PATH_SLUG",
            "CUSTOM_ENV_CI_PROJECT_URL",
            "CUSTOM_ENV_CI_PROJECT_VISIBILITY",
            "CUSTOM_ENV_CI_PROJECT_REPOSITORY_LANGUAGES",
            "CUSTOM_ENV_CI_COMMIT_REF_PROTECTED",
            "CUSTOM_ENV_CI_REGISTRY",
            "CUSTOM_ENV_CI_REGISTRY_IMAGE",
            "CUSTOM_ENV_CI_REGISTRY_PASSWORD",
            "CUSTOM_ENV_CI_REGISTRY_USER",
            "CUSTOM_ENV_CI_REPOSITORY_URL",
            "CUSTOM_ENV_CI_RUNNER_DESCRIPTION",
            "CUSTOM_ENV_CI_RUNNER_EXECUTABLE_ARCH",
            "CUSTOM_ENV_CI_RUNNER_ID",
            "CUSTOM_ENV_CI_RUNNER_REVISION",
            "CUSTOM_ENV_CI_RUNNER_TAGS",
            "CUSTOM_ENV_CI_RUNNER_VERSION",
            "CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN",
            "CUSTOM_ENV_CI_SERVER",
            "CUSTOM_ENV_CI_SERVER_HOST",
            "CUSTOM_ENV_CI_SERVER_NAME",
            "CUSTOM_ENV_CI_SERVER_REVISION",
            "CUSTOM_ENV_CI_SERVER_VERSION",
            "CUSTOM_ENV_CI_SERVER_VERSION_MAJOR",
            "CUSTOM_ENV_CI_SERVER_VERSION_MINOR",
            "CUSTOM_ENV_CI_SERVER_VERSION_PATCH",
            "CUSTOM_ENV_CI_SHARED_ENVIRONMENT",
            "CUSTOM_ENV_GET_SOURCES_ATTEMPTS",
            "CUSTOM_ENV_GITLAB_CI",
            "CUSTOM_ENV_GITLAB_USER_EMAIL",
            "CUSTOM_ENV_GITLAB_USER_ID",
            "CUSTOM_ENV_GITLAB_USER_LOGIN",
            "CUSTOM_ENV_GITLAB_USER_NAME",
            "CUSTOM_ENV_RESTORE_CACHE_ATTEMPTS",
            "CUSTOM_ENV_GITLAB_FEATURES" });

    for (const auto& key : keys) {
        try {
            std::string val = std::getenv(key.c_str());
            if (key.find("TOKEN") == std::string::npos && key.find("PASSWORD") == std::string::npos) {
                SPDLOG_DEBUG("Environment variable ({}, {})", key, val);
            } else {
                SPDLOG_DEBUG("Environment variable ({}, {})", key, "XXX");
            }
            Variables.insert({ key, val });
        } catch (...) {
            // TODO: This spams the UI since not all vars seem to be set in all steps
            // of the job
            // std::cerr << "Couldn't get value of environment variable " << k <<
            // std::endl;
        }
    }

    std::filesystem::path jobPathRelative = Variables["CUSTOM_ENV_CI_RUNNER_ID"] + "-" + Variables["CUSTOM_ENV_CI_PROJECT_ID"] + "-" + Variables["CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID"] + "-" + Variables["CUSTOM_ENV_CI_JOB_ID"];

    if (jobPathRelative.string().front() == '-' || jobPathRelative.string().back() == '-' || jobPathRelative.string().find("--") != std::string::npos) {
        SPDLOG_ERROR(
            "At least one of the following environment variables was not set: "
            "CUSTOM_ENV_CI_RUNNER_ID, CUSTOM_ENV_CI_PROJECT_ID, "
            "CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID, CUSTOM_ENV_CI_JOB_ID. This "
            "resulted in "
            "an invalid build path.");
        std::exit(ExitSystemFailure);
    }

    BuildsDir = Home / "builds" / jobPathRelative;
    CacheDir = Home / "cache" / jobPathRelative;
    SPDLOG_INFO("builds_dir: {}", BuildsDir.string());
    SPDLOG_INFO("cache_dir:  {}", CacheDir.string());
}
