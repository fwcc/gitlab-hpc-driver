#ifndef JOB_STAGE_H
#define JOB_STAGE_H

#include "CIJob.h"

#include <filesystem>
#include <string>

/**
 * @brief Class to represent a job of the run stage.
 *
 * This is meant to be used as a base class for \ref CIRunJobHead and \ref
 * CIRunJobCluster.
 */
class CIRunJob : public CIJob {

public:
    /**
     * @brief Simple constructor.
     * @param[in] script Sets #Script.
     * @param[in] subStage Sets #SubStage.
     */
    CIRunJob(Environment* env, std::filesystem::path script, std::string subStage)
        : CIJob(env)
        , Script(script)
        , SubStage(subStage)
    {
    }
    /**
     * @brief Simple destructor.
     */
    virtual ~CIRunJob() override = default;
    /**
     * @brief Method to be used to execute the job.
     */
    virtual int Run() override = 0;

    /**
     * @brief Simple getter.
     * @return #Script
     */
    std::filesystem::path GetScript() { return Script; }

    /**
     * @brief Simple getter.
     * @return #SubStage
     */
    std::string GetSubStage() { return SubStage; }

private:
    std::filesystem::path Script; ///< Path to the script to be executed.
    std::string SubStage; ///< The stage of #Script.
};

#endif // JOB_STAGE_H
