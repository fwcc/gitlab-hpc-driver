#include "CIJobFactory.h"

#include <algorithm>
#include <cstdlib>

#include "CICleanupJob.h"
#include "CIConfigJob.h"
#include "CIJob.h"
#include "CIRunJobCluster.h"
#include "CIRunJobHead.h"
#include "spdlog/spdlog.h"

CIJobFactory::CIJobFactory(Environment* env)
{
    if (env == nullptr)
        throw std::invalid_argument("env must not be nullptr!");
    Env = env;
}

CIJob*
CIJobFactory::CreateCIJob(const std::string& jobStage,
    const std::filesystem::path& script,
    const std::string& subStage,
    bool unifiedOutput)
{

    if (jobStage == "config")
        return new CIConfigJob(Env);

    else if (jobStage == "run") {

        if (script.empty()) {
            SPDLOG_ERROR("No script provided for run job.");
            return nullptr;
        } else {
            std::error_code ec;

            if (!std::filesystem::is_regular_file(script, ec)) {
                if (!ec) {
                    SPDLOG_ERROR("Provided script {} is not a regular file.",
                        script.string());
                } else {
                    SPDLOG_ERROR("Error when checking script: {}", ec.message());
                }
                return nullptr;
            }
        }

        if (subStage.empty()) {
            SPDLOG_ERROR("No substage provided for run job.");
            return nullptr;
        }

        if (subStage == "build_script" || subStage.find("step_") == 0)
            return new CIRunJobCluster(Env, script, subStage, unifiedOutput);
        else {
            std::array<std::string, 10> otherSubStages = {
                "prepare_script",
                "get_sources",
                "restore_cache",
                "download_artifacts",
                "after_script",
                "archive_cache",
                "archive_cache_on_failure",
                "upload_artifacts_on_success",
                "upload_artifacts_on_failure",
                "cleanup_file_variables"
            };

            if (std::find(otherSubStages.cbegin(), otherSubStages.cend(), subStage) != otherSubStages.cend())
                return new CIRunJobHead(Env, script, subStage);
            else {
                SPDLOG_ERROR("Unknown substage {} for run job.", subStage);
                return nullptr;
            }
        }
    }

    else if (jobStage == "cleanup")
        return new CICleanupJob(Env);

    SPDLOG_ERROR("Unknown job stage {}.", jobStage);
    return nullptr;
}
