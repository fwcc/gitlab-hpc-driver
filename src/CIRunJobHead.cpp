#include "CIRunJobHead.h"

#include <cstdlib>
#include <iostream>

#include "spdlog/spdlog.h"

CIRunJobHead::CIRunJobHead(Environment* env,
    std::filesystem::path script,
    std::string subStage)
    : CIRunJob(env, script, subStage)
{
    SPDLOG_INFO("New run job for head node with script {} of sub-stage {}",
        script.string(),
        subStage);
}

int CIRunJobHead::Run()
{
    int ret = std::system(CIRunJob::GetScript().c_str());
    if (ret == -1) {
        SPDLOG_ERROR("Couldn't run script {}", CIRunJob::GetScript().string());
        return CIJob::GetEnv()->GetExitSystemFailure();
    } else if (ret != 0) {
        SPDLOG_INFO("CI script {} returned non-zero exit code {}",
            CIRunJob::GetScript().string(),
            WEXITSTATUS(ret));
        return CIJob::GetEnv()->GetExitBuildFailure();
    }
    return 0;
}
