#ifndef CI_ENVIRONMENT_H
#define CI_ENVIRONMENT_H

#include "Environment.h"

#include <map>

/**
 * @brief Class to represent the CI environment.
 *
 * This class represents the environment the GitLab Runner service provides when
 * calling the HPC driver.
 */
class CIEnvironment : public Environment {

public:
    /**
     * @brief Simple constructor.
     */
    CIEnvironment();

    /**
     * @brief Simple getter.
     * @return #ExitSystemFailure
     */
    int GetExitSystemFailure() override { return ExitSystemFailure; }

    /**
     * @brief Simple getter.
     * @return #ExitBuildFailure
     */
    int GetExitBuildFailure() override { return ExitBuildFailure; }

    /**
     * @brief Simple getter.
     * @return #Home
     */
    std::filesystem::path GetHome() override { return Home; }

    /**
     * @brief Simple getter.
     * @return #BuildsDir
     */
    std::filesystem::path GetBuildsDir() override { return BuildsDir; }

    /**
     * @brief Simple getter.
     * @return #CacheDir
     */
    std::filesystem::path GetCacheDir() override { return CacheDir; }

    /**
     * @brief Getter for environment variables.
     * @param[in] key The name of the environment variable.
     * @return If it exists, the value of the environment variable \p key,
     * otherwise an empty string.
     */
    std::string GetVariable(std::string key) override { return Variables[key]; }

private:
    int ExitSystemFailure; ///< The exit code to use when the system fails.
    int ExitBuildFailure; ///< The exit code to use when the build fails.
    std::filesystem::path
        Home; ///< The path to the home directory of the current user.
    std::filesystem::path
        BuildsDir; ///< The path to the directory where builds are executed.
    std::filesystem::path CacheDir; ///< The path to the cache directory.
    std::map<std::string, std::string>
        Variables; /**< Containing the environment variables set by
                        the GitLab runner service. */
};

#endif // CI_ENVIRONMENT_H
