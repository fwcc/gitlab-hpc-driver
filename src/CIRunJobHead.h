#ifndef JOB_STAGE_LOCAL_H
#define JOB_STAGE_LOCAL_H

#include "CIRunJob.h"

#include <string>

/**
 * @brief Class to represent a job of the run stage to be executed on the
 * head/login node.
 *
 * This is meant for scripts of stages like "prepare_executor" or "get_sources"
 * that perform non-cluster-related tasks like cloning the repository or
 * printing the hostname. Distributed HPC tasks are represented by \ref
 * CIRunJobCluster.
 */
class CIRunJobHead : public CIRunJob {

public:
    /**
     * @brief Simple constructor.
     * @param[in] script The path to the script to be executed.
     * @param[in] subStage The stage of the script, e.g. "prepare_script",
     * "get_sources", ...
     */
    CIRunJobHead(Environment* env,
        std::filesystem::path script,
        std::string subStage);

    /**
     * @brief Simple destructor.
     */
    ~CIRunJobHead() override = default;

    /**
     * @brief Method used to execute the job.
     * @return The exit code of the job.
     */
    int Run() override;
};

#endif // JOB_STAGE_LOCAL_H
