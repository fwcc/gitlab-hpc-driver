#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <filesystem>
#include <string>

/**
 * @brief Base class to be able to easily mock CIEnvironment.
 */
class Environment {
public:
    virtual ~Environment() = default;
    virtual int GetExitSystemFailure() = 0;
    virtual int GetExitBuildFailure() = 0;
    virtual std::filesystem::path GetHome() = 0;
    virtual std::filesystem::path GetBuildsDir() = 0;
    virtual std::filesystem::path GetCacheDir() = 0;
    virtual std::string GetVariable(std::string key) = 0;
};

#endif // ENVIRONMENT_H
