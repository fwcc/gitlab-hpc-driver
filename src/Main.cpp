#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <memory>
#include <system_error>

#include "CIEnvironment.h"
#include "CIJobFactory.h"
#include "config.h"
#include "logging.h"
#include "spdlog/spdlog.h"

// TODO: document other possible return values.
/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 *     - On success: 0.
 *     - When the build system fails: The value defined by the environment
 * variable \c SYSTEM_FAILURE_EXIT_CODE.
 *     - When the build itself fails: The value defined by the environment
 * variable \c BUILD_FAILURE_EXIT_CODE.
 *     - When something goes wrong: One of many (hopefully) unique values (to be
 * documented).
 *
 * How this program is called:
 *
 * <tt>gitlab-hpc-driver (config | run \<script_path\> \<substage\> |
 * cleanup)</tt>
 */
int main(int argc, char* argv[])
{
    std::string h;
    try {
        h = std::getenv("HOME");
    } catch (...) {
        std::cerr << "Could not get home directory" << std::endl;
        return 50;
    }
    std::filesystem::path home(h);

    std::unique_ptr<libconfig::Config> config(config::make(home));
    if (config == nullptr)
        return 51;

    int result = logging::setup(config, home);
    if (result != 0)
        return result;
    SPDLOG_INFO("Started {}", program_invocation_name);

    CIEnvironment env;

    std::error_code ec;
    auto wd = std::filesystem::current_path(ec);
    if (ec) {
        SPDLOG_ERROR("Couldn't get current path: {}", ec.message());
        return env.GetExitSystemFailure();
    }
    SPDLOG_DEBUG("Current path: {}", wd.string());

    if (argc != 2 && argc != 4) {
        SPDLOG_ERROR("Wrong number of arguments. Expected call: {} (config | run "
                     "<script_path> "
                     "<substage> | cleanup)",
            program_invocation_name);
        return env.GetExitSystemFailure();
    }

    std::string jobStage = argc >= 2 ? argv[1] : "";
    std::string scriptName = argc >= 3 ? argv[2] : "";
    std::string subStage = argc >= 4 ? argv[3] : "";

    bool unifiedOutput = true;
    config->lookupValue("unified_output", unifiedOutput);

    CIJobFactory jf(&env);
    std::unique_ptr<CIJob> job(
        jf.CreateCIJob(jobStage, scriptName, subStage, unifiedOutput));

    if (job == nullptr)
        return env.GetExitSystemFailure();

    result = job->Run();

    return result;
}
