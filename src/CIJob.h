#ifndef CI_JOB_H
#define CI_JOB_H

#include "Environment.h"

/**
 * @brief Class to represent a job for the CI system.
 */
class CIJob {

public:
    /**
     * @brief Simple constructor.
     * @param env Sets #Env
     */
    CIJob(Environment* env)
        : Env(env)
    {
    }

    /**
     * @brief Simple destructor.
     */
    virtual ~CIJob() = default;

    /**
     * @brief Method that is called to run the job. Children of this class need to
     * implement this.
     * @return The exit code of the job.
     */
    virtual int Run() = 0;

    /**
     * @brief Simple getter.
     * @return #Env
     */
    Environment* GetEnv() { return Env; }

private:
    Environment* Env; ///< The environment provided by GitLab's CI.
};

#endif // CI_JOB_H
