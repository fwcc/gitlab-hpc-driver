#ifndef SLURM_JOB_H
#define SLURM_JOB_H

#include "HPCJob.h"

#include <array>
#include <filesystem>
#include <fstream>
#include <future>
#include <map>
#include <mutex>
#include <string>
#include <utility>

/**
 * @brief Class to represent a Slurm job.
 */
class SlurmJob : public HPCJob {

public:
    /**
     * @brief Constructor. Also prepares the script to be executed by adding Slurm
     * specific options.
     * @param[in] script Path of the base script for #Script.
     * @param[in] unifiedOutput Sets #UnifiedOutput.
     */
    SlurmJob(Environment* env,
        std::filesystem::path script,
        bool unifiedOutput = true);

    /**
     * @brief The method to be used to execute the job.
     * @return The exit code of the job.
     */
    int Run() override;

    /**
     * @brief The method to be used to cancel the job.
     */
    void Cancel() override;

private:
    /**
     * @brief Submits #Script as a batch job.
     */
    int SubmitBatch();

    /**
     * @brief Updates the state of the Slurm job representation.
     */
    void UpdateState();

    /**
     * @brief Prepares #Script based on \p oldScript.
     * @param[in] oldScript
     */
    void PrepareNewScript(std::filesystem::path oldScript);

    /**
     * @brief Extracts Slurm options from \p oldScript and adds them to #Settings.
     * @param[in] oldScript
     */
    void ExtractSettings(std::filesystem::path oldScript);

    /**
     * @brief Outputs a given file until \p killSig is set.
     * @param[out] out The stream to output the file to.
     * @param[in] fileFame The path to the file to be outputted.
     * @param[in] killSig Signals when to stop the output.
     */
    void OutputFile(std::ostream& out,
        const std::string& fileName,
        std::shared_future<void> killSig);

    /**
     * @brief Executes a given shell command.
     * @param[in] command The command to be executed.
     * @return The output of the command.
     */
    std::pair<int, std::string> Execute(const std::string& command);

    bool UnifiedOutput; ///< Defines whether to unify stdout and stderr.
    std::map<std::string, std::string>
        Settings; ///< Contains the specified Slurm options.
    std::filesystem::path Script; ///< The path of the script to be executed.
    std::string ID; ///< The ID of the Slurm job.
    std::string
        State; ///< The state of the Slurm job: pending, running, canceled, ...
    std::string
        ExitCode; /**< The exit code Slurm returns after running #script. In the
                     same notation as when calling \c scontrol, e.g. "0:0". */
    std::filesystem::path
        OutFile; ///< The path to the output file written by Slurm.
    std::filesystem::path
        ErrFile; ///< The path to the error file written by Slurm.

    static std::string YmlToBash(std::string s);

    static inline std::mutex OutputLock;

    static inline std::array<std::string, 1> SuccessStates = { "COMPLETED" };
    static inline std::array<std::string, 5> FailStatesJob = { "CANCELLED",
        "DEADLINE",
        "FAILED",
        "OUT_OF_MEMORY",
        "TIMEOUT" };
    static inline std::array<std::string, 3> FailStatesSystem = {
        "BOOT_FAIL",
        "NODE_FAIL",
        "SPECIAL_EXIT" // This can mean different things, depending on the setup.
    };
    static inline std::array<std::string, 16> InProgressStates = {
        "CONFIGURING", "COMPLETING", "PENDING", "PREEMPTED", "RUNNING",
        "RESV_DEL_HOLD", "REQUEUE_FED", "REQUEUE_HOLD", "REQUEUED", "RESIZING",
        "REVOKED", "SIGNALING", "STAGE_OUT", "STOPPED", "SUSPENDED",
        "_UNKNOWN_" // Placeholder for when Slurm is not reachable
    };
};

#endif // SLURM_JOB_H
