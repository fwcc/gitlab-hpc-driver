#include "CICleanupJob.h"

#include <cstdlib>
#include <filesystem>
#include <string>
#include <system_error>

#include "spdlog/spdlog.h"

CICleanupJob::CICleanupJob(Environment* env)
    : CIJob(env)
{
    SPDLOG_INFO("New cleanup job");
}

int CICleanupJob::Run()
{
    std::error_code ec;
    std::filesystem::remove_all(CIJob::GetEnv()->GetBuildsDir(), ec);

    if (ec) {
        SPDLOG_ERROR("Error during deletion of build directory {}: {}",
            CIJob::GetEnv()->GetBuildsDir().string(),
            ec.message());
        return CIJob::GetEnv()->GetExitSystemFailure();
    }
    return 0;
}
