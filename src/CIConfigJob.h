#ifndef CI_CONFIG_JOB_H
#define CI_CONFIG_JOB_H

#include "CIJob.h"

/**
 * @brief Class to represent a job of the config stage.
 */
class CIConfigJob : public CIJob {

public:
    /**
     * @brief Simple constructor.
     * @param[in] env The environment provided by GitLab's CI.
     */
    CIConfigJob(Environment* env);

    /**
     * @brief Simple destructor.
     */
    ~CIConfigJob() override = default;

    /**
     * @brief Method used to execute the job.
     * @return The exit code of the job.
     */
    int Run() override;
};

#endif // CI_CONFIG_JOB_H
