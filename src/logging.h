#ifndef LOGGING_H
#define LOGGING_H

#include <filesystem>

#include "CIEnvironment.h"
#include "libconfig.h++"

/**
 * @brief Functions related to logging.
 */
namespace logging {

/**
 * @brief Sets up logging (log level, logs directory, ...).
 * @param[in] config A \c libconfig::Config from which the path for log files
 * and the log level are read.
 * @param[in] home The path to the home directory; ~/logs is used as a fallback
 * when no other logging directory is specified.
 * @return 0 on success, a non-zero number on failure.
 */
int setup(const std::unique_ptr<libconfig::Config>& config,
    const std::filesystem::path& home);

} // namespace logging

#endif // LOGGING_H
