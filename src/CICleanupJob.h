#ifndef CI_CLEANUP_JOB_H
#define CI_CLEANUP_JOB_H

#include "CIJob.h"

/**
 * @brief Class to represent a job of the cleanup stage.
 */
class CICleanupJob : public CIJob {

public:
    /**
     * @brief Simple constructor.
     * @param[in] env The environment provided by GitLab's CI.
     */
    CICleanupJob(Environment* env);

    /**
     * @brief Method used to execute the job.
     * @return The exit code of the job.
     */
    int Run() override;
};

#endif // CI_CLEANUP_JOB_H
