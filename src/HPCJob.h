#ifndef HPC_JOB_H
#define HPC_JOB_H

#include <string>

#include "Environment.h"

/**
 * @brief Class representing an HPC job.
 *
 * This is meant to be used as base class for \ref SlurmJob and classes for
 * various other batch systems that may be implemented in the future.
 */
class HPCJob {

public:
    /**
     * @brief Simple constructor
     * @param env Sets #Env.
     */
    HPCJob(Environment* env)
        : Env(env)
    {
    }

    /**
     * @brief Simple destructor.
     */
    virtual ~HPCJob() = default;

    /**
     * @brief Method to be used to execute the job.
     * @return The exit code of the job.
     */
    virtual int Run() = 0;

    /**
     * @brief Method to be used to cancel the job.
     */
    virtual void Cancel() = 0;

    /**
     * @brief Simple getter.
     * @return #Env
     */
    Environment* GetEnv() { return Env; }

private:
    Environment* Env; ///< The environment provided by the GitLab CI.
};

#endif // HPC_JOB_H
