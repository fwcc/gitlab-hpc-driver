#include "logging.h"

#include <algorithm>
#include <array>
#include <cerrno>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "libconfig.h++"
#include "spdlog/sinks/daily_file_sink.h"
#include "spdlog/spdlog.h"

static std::array<std::string, 8> logLevels = { "trace", "debug", "info",
    "warning", "error", "critical",
    "off", "null" };

int logging::setup(const std::unique_ptr<libconfig::Config>& config,
    const std::filesystem::path& home)
{
    std::filesystem::path logsDir = home / "logs";
    std::string logsDirS;
    config->lookupValue("logging_directory", logsDirS);
    if (!logsDirS.empty())
        logsDir = std::filesystem::path(logsDirS);

    int fail = ::mkdir(logsDir.c_str(), S_IRWXU | S_IRGRP | S_IXGRP); // drwxr-x---
    if (fail && errno != EEXIST) {
        std::cerr << "Couldn't create logging directory " << logsDir << std::endl;
        return 42;
    }
    pid_t pid = ::getpid();

    try {
        auto logger = spdlog::daily_logger_mt("GitLab HPC Driver (" + std::to_string(pid) + ")",
            (logsDir / "hpc-driver.log").string());
        spdlog::set_default_logger(logger);
    } catch (const spdlog::spdlog_ex& ex) {
        // TODO: Better solution?
        std::cerr << "Log init failed: " << ex.what() << std::endl;
        return 43;
    }

    std::string logLevel = "info";
    config->lookupValue("log_level", logLevel);
    if (std::find(logLevels.begin(), logLevels.end(), logLevel) != logLevels.end())
        spdlog::set_level(spdlog::level::from_str(logLevel));
    else
        spdlog::set_level(spdlog::level::info);

    return 0;
}
