#ifndef JOB_STAGE_REMOTE_H
#define JOB_STAGE_REMOTE_H

#include "CIRunJob.h"

#include <map>
#include <string>

#include "HPCJob.h"

/**
 * @brief Class to represent a job of the run stage to be executed distributedly
 * on the cluster.
 *
 * This is meant for scripts of the substage "build_script". Non-cluster-related
 * tasks are represented by \ref CIRunJobHead.
 */
class CIRunJobCluster : public CIRunJob {

public:
    /**
     * @brief Constructor. Also registers #handler as signal handler for \c
     * SIGTERM and sets #instance.
     * @param[in] script Sets \ref CIRunJob::script.
     * @param[in] substage Sets \ref CIRunJob::substage.
     * @param[in] unifiedOutput Sets #UnifiedOutput.
     */
    CIRunJobCluster(Environment* env,
        std::filesystem::path script,
        std::string subStage,
        bool unifiedOutput = true);

    /**
     * @brief Simple destructor.
     */
    ~CIRunJobCluster() override = default;

    /**
     * @brief Method used to execute the job.
     * @return The exit code of the job.
     */
    int Run() override;

private:
    HPCJob* Job; ///< A pointer to the corresponding HPC job for the cluster.
    bool UnifiedOutput; ///< Defines whether to unify stdout/stderr.

    /**
     * @brief Helper function, converting std::string.
     * @param[in] s The std::string to be converted.
     * @return The converted std::string.
     */
    static std::string YmlToBash(std::string s);
    /**
     * @brief Signal handler, cancels #job on \c SIGTERM.
     * @param sig The received signal.
     */
    static void Handler(int sig);

    static inline CIRunJobCluster* Instance = nullptr; /**< A pointer to an instance of this class
                                                            that can be used by #Handler to find the
                                                            object. */
};

#endif // JOB_STAGE_REMOTE_H
