#ifndef CONFIG_H
#define CONFIG_H

#include <filesystem>

#include "libconfig.h++"

namespace config {

libconfig::Config*
make(const std::filesystem::path& home);

}

#endif // CONFIG_H
