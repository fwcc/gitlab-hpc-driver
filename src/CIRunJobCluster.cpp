#include "CIRunJobCluster.h"

#include <csignal>

#include "SlurmJob.h"
#include "spdlog/spdlog.h"

CIRunJobCluster::CIRunJobCluster(Environment* env,
    std::filesystem::path script,
    std::string subStage,
    bool unifiedOutput)
    : CIRunJob(env, script, subStage)
    , UnifiedOutput(unifiedOutput)
{
    Instance = this;
    std::signal(SIGTERM, Handler);
    SPDLOG_INFO("New run job for cluster with script {} of sub-stage {}",
        script.string(),
        subStage);
}

int CIRunJobCluster::Run()
{
    // TODO: When supporting more than one batch system:
    // Decide which batch system is used (based on variables in script / programs
    // installed) and create an appropriate job for it.

    // Job needs to be accessible via job variable for the signal handler to find
    // it!
    Job = new SlurmJob(GetEnv(), CIRunJob::GetScript(), UnifiedOutput);
    int result = Job->Run();
    delete Job;
    Job = nullptr;

    return result;
}

void CIRunJobCluster::Handler(int sig)
{
    if (sig == SIGTERM) {
        SPDLOG_INFO("Received SIGTERM during running HPC job ...");
        if (Instance->Job != nullptr) {
            SPDLOG_INFO("Cancelling HPC job");
            Instance->Job->Cancel();
            delete Instance->Job;
            Instance->Job = nullptr;
        } else
            SPDLOG_INFO("No HPC job to cancel");

        std::exit(Instance->GetEnv()->GetExitBuildFailure());
    }
}
