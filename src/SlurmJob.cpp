#include "SlurmJob.h"

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <regex>
#include <sstream>
#include <thread>
#include <vector>

#include "spdlog/spdlog.h"

SlurmJob::SlurmJob(Environment* env,
    std::filesystem::path script,
    bool unifiedOutput)
    : HPCJob(env)
    , UnifiedOutput(unifiedOutput)
    , Script(script.string() + ".slurm")
{
    PrepareNewScript(script);
}

int SlurmJob::SubmitBatch()
{
    std::string sbatchCommand = "sbatch " + Script.string();
    auto results = Execute(sbatchCommand);

    if (results.first != 0) {
        SPDLOG_ERROR(results.second);
        return results.first;
    }

    std::istringstream issOutput(results.second);
    std::string a, b, c, slurmJobID;
    issOutput >> a >> b >> c >> ID;

    // Update out- and err_file by replacing %j with the actual job ID
    auto outFileStr = OutFile.string();
    auto errFileStr = ErrFile.string();
    OutFile = outFileStr.replace(
        outFileStr.end() - 6, outFileStr.end() - 4, ID.c_str(), ID.size());
    ErrFile = errFileStr.replace(
        errFileStr.end() - 6, errFileStr.end() - 4, ID.c_str(), ID.size());

    return results.first;
}

int SlurmJob::Run()
{
    int result = SubmitBatch();
    if (result != 0)
        return result;

    std::promise<void> killSigP;
    auto killSigF = killSigP.get_future().share();
    // Returned futures have to be stored, otherwise their destructors will be
    // called instantly and block
    std::vector<std::future<void>> fs;

    fs.push_back(std::async(std::launch::async,
        &SlurmJob::OutputFile,
        this,
        std::ref(std::cout),
        OutFile,
        killSigF));

    if (!UnifiedOutput)
        fs.push_back(std::async(std::launch::async,
            &SlurmJob::OutputFile,
            this,
            std::ref(std::cerr),
            ErrFile,
            killSigF));

    UpdateState();

    while (std::find(InProgressStates.cbegin(), InProgressStates.cend(), State) != InProgressStates.cend()) {
        {
            std::lock_guard guard(OutputLock);
            std::cerr << "\rSlurm job (" << ID << ") state: " << State << std::flush;
        }
        std::this_thread::sleep_for(std::chrono::seconds(5));
        UpdateState();
    }
    killSigP.set_value();

    {
        std::lock_guard guard(OutputLock);
        std::cerr << "\rSlurm job (" << ID << ") state: " << State
                  << ", exit code: " << ExitCode << std::endl;
    }

    // TODO: Differentiate between job states and correctly exit with system
    // failure or build failure (when users fault)
    if (std::find(SuccessStates.cbegin(), SuccessStates.cend(), State) == SuccessStates.cend()) {
        SPDLOG_INFO(
            "Slurm job with ID {} has not completed. State {}, exit code {}",
            ID,
            State,
            ExitCode);

        if (std::find(FailStatesJob.cbegin(), FailStatesJob.cend(), State) != FailStatesJob.cend())
            return HPCJob::GetEnv()->GetExitBuildFailure();
        else
            return HPCJob::GetEnv()->GetExitSystemFailure();
    }
    return 0;
}

void SlurmJob::Cancel()
{
    Execute("scancel " + ID);
}

void SlurmJob::PrepareNewScript(std::filesystem::path oldScript)
{
    std::filesystem::path buildsDir = HPCJob::GetEnv()->GetBuildsDir();
    OutFile = buildsDir / "slurm-%j.out";
    ErrFile = buildsDir / "slurm-%j.err";

    Settings.insert(
        { { "chdir", buildsDir.string() }, { "output", OutFile.string() } });
    if (!UnifiedOutput)
        Settings.insert({ "error", ErrFile.string() });

    ExtractSettings(oldScript);

    std::ifstream oldScriptS(oldScript, std::ios::in);
    std::ofstream newScriptS(Script, std::ios::out | std::ios::trunc);

    std::string shebang;
    std::getline(oldScriptS, shebang);

    // Print to new script
    newScriptS << shebang << '\n';
    for (auto setting : Settings)
        newScriptS << "#SBATCH --" << setting.first << "=" << setting.second
                   << '\n';

    newScriptS << "\n\n";
    newScriptS << oldScriptS.rdbuf();

    oldScriptS.close();
    newScriptS.close();
}

void SlurmJob::ExtractSettings(std::filesystem::path oldScript)
{
    std::ifstream script(oldScript);
    std::string scriptLine;

    while (getline(script, scriptLine)) {
        if (scriptLine.find(": | eval") == 0) {
            /*
             * TODO: Better solution?
             *
             * Numerical values will end up in the second capture group, strings like
             * "gpu:2" in the third one. In the latter case the second group will
             * contain an escaped bash string. The key is always stored in the first
             * capture group. Examples:
             *
             *   #1              |  #2          |  #3
             * ------------------|--------------|---------
             *   NTASKS          |  2           |
             *   CPUS_PER_TASKS  |  16          |
             *   PARTITION       |  $\'gpu\'    |  gpu
             *   GRES            |  $\'gpu:1\'  |  gpu:1
             */

            std::regex re(
                "CI_SLURM_([A-Z][_A-Z]*)=(\\$\\\\'([A-Za-z0-9,.:_]+)\\\\'|\\d+)");
            std::smatch match;
            std::string::const_iterator searchStart(scriptLine.cbegin());

            while (regex_search(searchStart, scriptLine.cend(), match, re)) {
                std::string key = YmlToBash(match.str(1));
                std::string val;

                if (match.str(3).empty())
                    val = match.str(2);
                else
                    val = match.str(3);

                if (key == "CI_SLURM_OUTPUT" || key == "CI_SLURM_ERROR" || key == "CI_SLURM_WORKDIR" || key == "CI_SLURM_CHDIR" || key == "CI_SLURM_DEPENDENCY") {
                    SPDLOG_DEBUG("Ignoring sbatch setting ({}, {}).", key, val);
                } else {
                    SPDLOG_DEBUG("sbatch setting found in CI script ({}, {})", key, val);
                    Settings.insert({ key, val });
                }

                searchStart = match.suffix().first;
            }

            break;
        }
    }

    script.close();
}

void SlurmJob::UpdateState()
{
    std::string scontrolCommand = "scontrol show job=" + ID;
    auto results = Execute(scontrolCommand);

    if (results.first != 0) {
        SPDLOG_ERROR(results.second);
        State = "_UNKNOWN_";
        return;
    }

    std::istringstream issOutput(results.second);
    std::string line;
    for (int i = 0; i < 3; i++)
        std::getline(issOutput, line);

    std::string jobStateField;
    issOutput >> jobStateField;

    std::getline(issOutput, line);
    std::string exitCodeField;
    for (int i = 0; i < 5; i++)
        issOutput >> exitCodeField;

    State = jobStateField.substr(9);
    ExitCode = exitCodeField.substr(9);
}

std::pair<int, std::string>
SlurmJob::Execute(const std::string& command)
{
    const char* cmd = command.c_str();

    std::array<char, 32> buffer;
    int result = 0;
    std::string output;
    std::FILE* pipe = ::popen(cmd, "r");
    if (!pipe) {
        result = HPCJob::GetEnv()->GetExitSystemFailure();
        output = "popen() failed when trying to execute " + command;
        return std::make_pair(result, output);
    }
    while (std::fgets(buffer.data(), buffer.size(), pipe) != nullptr)
        output += buffer.data();
    if (::pclose(pipe)) {
        result = HPCJob::GetEnv()->GetExitSystemFailure();
        output = "Command failed: " + command;
    }
    return make_pair(result, output);
}

// TODO: Use inotify/... to be notified on new data instead of trying in a loop?
void SlurmJob::OutputFile(std::ostream& out,
    const std::string& fileName,
    std::shared_future<void> killSig)
{
    SPDLOG_DEBUG("Outputting file {} in new thread{}",
        fileName,
        UnifiedOutput ? " (unified output)" : "");

    // Make sure the file exists
    std::ofstream outFile(fileName);
    outFile.close();

    std::ifstream file(fileName);
    // TODO: Good timing to compromise between low overhead and "live" output?
    // (Slurm buffers its output anyway, so ...)
    while (killSig.wait_for(std::chrono::milliseconds(100)) == std::future_status::timeout) {
        std::string line;
        while (std::getline(file, line)) {
            std::lock_guard guard(OutputLock);
            out << '\r' << line << std::endl;
        }
        file.clear();
    }
    SPDLOG_DEBUG("Output job for {} received kill signal. Finalizing ...",
        fileName);
    out << file.rdbuf();

    file.close();
}

std::string
SlurmJob::YmlToBash(std::string s)
{
    transform(s.begin(), s.end(), s.begin(), [](unsigned char c) {
        return c == '_' ? '-' : tolower(c);
    });
    return s;
}
