#ifndef CI_JOB_FACTORY_H
#define CI_JOB_FACTORY_H

#include <filesystem>
#include <string>

#include "CIJob.h"
#include "Environment.h"

/**
 * @brief Factory for \ref CIJob objects.
 *
 * Used to create objects of classes based on \ref CIJob. The factory chooses
 * the appropiate subclass based on the given arguments.
 */
class CIJobFactory {

public:
    /**
     * @brief Simple constructor.
     * @param[in] env Sets #Env.
     */
    CIJobFactory(Environment* env);

    /**
     * @brief Creates a \ref CIJob.
     * @param[in] jobStage Stage that the \ref CIJob to be created belongs to.
     * @param[in] script The path to the script to be executed by the \ref CIJob.
     * @param[in] subStage Sub-stage that the passed script belongs to.
     * @param[in] unifiedOutput Defines whether to unify stdout/stderr.
     * @return A \ref CIJob matching the given parameters.
     */
    CIJob* CreateCIJob(const std::string& jobStage,
        const std::filesystem::path& script = "",
        const std::string& subStage = "",
        bool unifiedOutput = true);

private:
    Environment* Env; ///< The environment provided by GitLab's CI.
};

#endif // CI_JOB_FACTORY_H
