#include "config.h"

#include <filesystem>
#include <iostream>

libconfig::Config*
config::make(const std::filesystem::path& home)
{
    auto config = new libconfig::Config();

    std::filesystem::path cfgFile = home / ".config" / (program_invocation_short_name + std::string(".cfg"));

    try {
        config->readFile(cfgFile.c_str());
    } catch (const libconfig::FileIOException&) {
        delete config;
        return new libconfig::Config();
    } catch (const libconfig::ParseException& pex) {
        std::cerr << "Error parsing config file at " << pex.getFile() << ":"
                  << pex.getLine() << ": " << pex.getError() << std::endl;
        return nullptr;
    }

    return config;
}
