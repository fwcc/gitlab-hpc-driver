#include "CIConfigJob.h"

#include <filesystem>
#include <iostream>

#include "spdlog/spdlog.h"

CIConfigJob::CIConfigJob(Environment* env)
    : CIJob(env)
{
    SPDLOG_INFO("New config job");
}

int CIConfigJob::Run()
{
    std::cout << "{\n"
                 "    \"builds_dir\": \""
            + CIJob::GetEnv()->GetBuildsDir().string() + "\",\n"
                                                         "    \"cache_dir\": \""
            + CIJob::GetEnv()->GetCacheDir().string() + "\"\n"
                                                        "}"
              << std::endl;

    return 0;
}
