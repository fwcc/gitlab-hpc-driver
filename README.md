# GitLab HPC Driver

HPC driver to be used with GitLab CI custom executor. The initial implementation will support Slurm
and might be generalized in the future.

## License

This software is licensed under [GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html). The
[CodeCoverage.cmake](cmake/CodeCoverage.cmake) script has a different license --- see the file
for more information.

## Documentation

A user guide on the runner using this driver can be found on
[InfoHub](http://fwcc.pages.hzdr.de/infohub/software/gitlab/ci.html#experimental-hpc-runner).
Developers and system administrators can find documentation for the master branch on
[GitLab Pages](http://fwcc.pages.hzdr.de/gitlab-hpc-driver/). For any other branch the documentation
can be built by following the instructions below.

## Build instructions

Required programs and libraries:

- A current GCC version that supports C++17 (Clang and other compilers should work as well but the
  test coverage target won't with them since it uses GCOV)
- GNU Make and CMake
- [Google Test](https://github.com/google/googletest) and Google Mock
- [spdlog](https://github.com/gabime/spdlog)
- [libconfig](https://github.com/hyperrealm/libconfig)
- lcov (for test coverage)

To generate the documentation you need:
- Doxygen
- Graphviz
- [Sphinx](https://github.com/sphinx-doc/sphinx/)
- Sphinx Read the Docs theme
- [Breathe](https://github.com/michaeljones/breathe)
- [Exhale](https://github.com/svenevs/exhale)

To build the program execute the following commands in the repository's directory:

```bash
mkdir build && cd build
cmake ..
make
make test       # to run the tests
make install    # to install the program in <install-prefix>/bin
make docs       # to generate the documentation in ./docs
```

A different installation path can be set with the option `-DCMAKE_INSTALL_PREFIX=` when calling
`cmake`.
