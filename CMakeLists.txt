cmake_minimum_required(VERSION 3.13)

project(gitlab-hpc-driver
    VERSION 0.1.1
    LANGUAGES CXX
)

add_compile_options(-Wall -Wextra)

# TODO: Change once `add_compile_features` is a thing. Only exists for targets atm :-(
# add_compile_features(cxx_std_17)
set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        OFF)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake" "${CMAKE_MODULE_PATH}")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

find_package     (Threads   REQUIRED)
find_package     (spdlog    REQUIRED)
find_package     (PkgConfig REQUIRED)
pkg_check_modules(CONFIG++  REQUIRED IMPORTED_TARGET libconfig++)
find_package     (GTest)
pkg_check_modules(GMOCK              IMPORTED_TARGET gmock)
find_package     (Doxygen)
find_package     (Sphinx)

include_directories(src)

add_subdirectory(src)

if("${GTEST_FOUND}" AND "${GMOCK_FOUND}")
    enable_testing()
    add_subdirectory(tests)
endif()

if("${DOXYGEN_FOUND}" AND "${SPHINX_FOUND}")
    add_subdirectory(docs)
endif()
