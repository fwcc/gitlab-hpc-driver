#ifndef MOCKCIENVIRONMENT_H
#define MOCKCIENVIRONMENT_H

#include "Environment.h"

#include "gmock/gmock.h"

class MockCIEnvironment : public Environment {
public:
    MOCK_METHOD(int, GetExitSystemFailure, (), (override));
    MOCK_METHOD(int, GetExitBuildFailure, (), (override));
    MOCK_METHOD(std::filesystem::path, GetHome, (), (override));
    MOCK_METHOD(std::filesystem::path, GetBuildsDir, (), (override));
    MOCK_METHOD(std::filesystem::path, GetCacheDir, (), (override));
    MOCK_METHOD(std::string, GetVariable, (std::string), (override));
};

#endif // MOCKCIENVIRONMENT_H
