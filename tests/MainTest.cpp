#include <cstdlib>

#include "spdlog/spdlog.h"
#include "gtest/gtest.h"

#include <iostream>

using ::testing::Test;

class MainTest : public Test {
protected:
    MainTest()
    {
        ::setenv("BUILD_FAILURE_EXIT_CODE", "1", 1);
        ::setenv("SYSTEM_FAILURE_EXIT_CODE", "2", 1);
        ::setenv("CUSTOM_ENV_CI_RUNNER_ID", "1", 1);
        ::setenv("CUSTOM_ENV_CI_PROJECT_ID", "2", 1);
        ::setenv("CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID", "3", 1);
        ::setenv("CUSTOM_ENV_CI_JOB_ID", "4", 1);
    }
};

int main_tbt(int argc, char* argv[]);

TEST_F(MainTest, Args1)
{
    char* args[] = { "gitlab-hpc-driver" };
    EXPECT_EQ(2, main_tbt(1, args));
    spdlog::drop_all();
}

TEST_F(MainTest, Args2foo)
{
    char* args[] = { "gitlab-hpc-driver", "foo" };

    EXPECT_EQ(2, main_tbt(2, args));
    spdlog::drop_all();
}

TEST_F(MainTest, Args2config)
{
    char* args[] = { "gitlab-hpc-driver", "config" };

    EXPECT_EQ(0, main_tbt(2, args));
    spdlog::drop_all();
}

TEST_F(MainTest, Args3)
{
    char* args[] = { "gitlab-hpc-driver", "foo", "bar" };

    EXPECT_EQ(2, main_tbt(3, args));
    spdlog::drop_all();
}

TEST_F(MainTest, Args4)
{
    char* args[] = { "gitlab-hpc-driver", "foo", "bar", "baz" };

    EXPECT_EQ(2, main_tbt(4, args));
    spdlog::drop_all();
}
