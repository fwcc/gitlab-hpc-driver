#include "SlurmJob.h"
#include "MockCIEnvironment.h"
#include "gtest/gtest.h"

using ::testing::Return;

TEST(SlurmJobTest, Run)
{
    MockCIEnvironment env;
    EXPECT_CALL(env, GetBuildsDir())
        .Times(2)
        .WillRepeatedly(Return(std::filesystem::path(DUMMY_BUILDS_PATH)));
    EXPECT_CALL(env, GetExitSystemFailure()).WillRepeatedly(Return(2));

    SlurmJob job1(&env, DUMMY_SCRIPT, true);
    SlurmJob job2(&env, DUMMY_SCRIPT, false);

    EXPECT_EQ(0, job1.Run());
    EXPECT_EQ(0, job2.Run());
}
