#include "CIJobFactory.h"
#include "CICleanupJob.h"
#include "CIConfigJob.h"
#include "CIRunJob.h"
#include "CIRunJobCluster.h"
#include "CIRunJobHead.h"
#include "MockCIEnvironment.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using ::testing::Test;

class CIJobFactoryCreateJobTest : public Test {
protected:
    CIJobFactoryCreateJobTest()
        : JobFactory(new MockCIEnvironment)
    {
    }
    CIJobFactory JobFactory;
};

TEST_F(CIJobFactoryCreateJobTest, FooBarJob)
{
    auto job1 = JobFactory.CreateCIJob("foobar");
    auto job2 = JobFactory.CreateCIJob("foobar", "foo");
    auto job3 = JobFactory.CreateCIJob("foobar", "bar", "baz");
    EXPECT_EQ(nullptr, job1);
    EXPECT_EQ(nullptr, job2);
    EXPECT_EQ(nullptr, job3);
}

TEST_F(CIJobFactoryCreateJobTest, ConfigJob)
{
    auto job1 = JobFactory.CreateCIJob("config");
    auto job2 = JobFactory.CreateCIJob("config", "foo");
    auto job3 = JobFactory.CreateCIJob("config", "bar", "baz");
    EXPECT_NE(nullptr, job1);
    EXPECT_NE(nullptr, job2);
    EXPECT_NE(nullptr, job3);

    EXPECT_NE(nullptr, dynamic_cast<CIConfigJob*>(job1));
    EXPECT_NE(nullptr, dynamic_cast<CIConfigJob*>(job2));
    EXPECT_NE(nullptr, dynamic_cast<CIConfigJob*>(job3));
}

TEST_F(CIJobFactoryCreateJobTest, CleanupJob)
{
    auto job1 = JobFactory.CreateCIJob("cleanup");
    auto job2 = JobFactory.CreateCIJob("cleanup", "foo");
    auto job3 = JobFactory.CreateCIJob("cleanup", "bar", "baz");
    EXPECT_NE(nullptr, job1);
    EXPECT_NE(nullptr, job2);
    EXPECT_NE(nullptr, job3);

    EXPECT_NE(nullptr, dynamic_cast<CICleanupJob*>(job1));
    EXPECT_NE(nullptr, dynamic_cast<CICleanupJob*>(job2));
    EXPECT_NE(nullptr, dynamic_cast<CICleanupJob*>(job3));
}

TEST_F(CIJobFactoryCreateJobTest, RunJob)
{
    auto job01 = JobFactory.CreateCIJob("run");
    auto job02 = JobFactory.CreateCIJob("run", "foo");
    auto job03 = JobFactory.CreateCIJob("run", "bar", "baz");
    EXPECT_EQ(nullptr, job01);
    EXPECT_EQ(nullptr, job02);
    EXPECT_EQ(nullptr, job03);

    auto job04 = JobFactory.CreateCIJob("run", "foo", "prepare_script");
    auto job05 = JobFactory.CreateCIJob("run", "foo", "build_script");
    auto job15 = JobFactory.CreateCIJob("run", "foo", "step_release");
    auto job06 = JobFactory.CreateCIJob("run", "foo", "archive_cache");
    auto job07 = JobFactory.CreateCIJob("run", "foo", "upload_artifacts_on_failure");
    EXPECT_EQ(nullptr, job04);
    EXPECT_EQ(nullptr, job05);
    EXPECT_EQ(nullptr, job06);
    EXPECT_EQ(nullptr, job07);
    EXPECT_EQ(nullptr, job15);

    auto job08 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "prepare_script");
    auto job09 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "build_script");
    auto job16 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "step_release");
    auto job10 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "archive_cache");
    auto job11 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "upload_artifacts_on_failure");
    EXPECT_NE(nullptr, dynamic_cast<CIRunJob*>(job08));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJob*>(job09));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJob*>(job16));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJob*>(job10));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJob*>(job11));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJobHead*>(job08));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJobCluster*>(job09));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJobHead*>(job10));
    EXPECT_NE(nullptr, dynamic_cast<CIRunJobHead*>(job11));

    auto job12 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT);
    auto job13 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "");
    auto job14 = JobFactory.CreateCIJob("run", DUMMY_SCRIPT, "foo");
    EXPECT_EQ(nullptr, job12);
    EXPECT_EQ(nullptr, job13);
    EXPECT_EQ(nullptr, job14);
}

TEST(CIJobFactoryTest, Constructor)
{
    EXPECT_THROW(CIJobFactory(nullptr), std::invalid_argument);
}
