#include <cstdlib>

#include "CIEnvironment.h"
#include "gtest/gtest.h"

using ::testing::ExitedWithCode;

TEST(CIEnvironmentTest, Constructor)
{
    EXPECT_EXIT(CIEnvironment(), ExitedWithCode(2), ".*");

    ::setenv("BUILD_FAILURE_EXIT_CODE", "1", 1);
    ::setenv("SYSTEM_FAILURE_EXIT_CODE", "2", 1);
    ::setenv("CUSTOM_ENV_CI_RUNNER_ID", "1", 1);
    ::setenv("CUSTOM_ENV_CI_PROJECT_ID", "2", 1);
    ::setenv("CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID", "3", 1);
    ::setenv("CUSTOM_ENV_CI_JOB_ID", "4", 1);

    CIEnvironment();
}
